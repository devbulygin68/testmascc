package com.example.testmascc.map;

import com.example.testmascc.dto.CartItemDto;
import com.example.testmascc.entity.CartDb;
import com.example.testmascc.entity.CartItemDb;
import com.example.testmascc.entity.ProductDb;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CartItemMapper implements Mapper<CartItemDb, CartItemDto> {
    private final CartMapper cartMapper;
    private final ProductMapper productMapper;

    @Override
    public CartItemDto map(CartItemDb object) {
        CartDb cart = object.getCart();
        ProductDb product = object.getProduct();

        return new CartItemDto(
                cartMapper.map(cart),
                productMapper.map(product),
                object.getQuantity()
        );
    }


}
