package com.example.testmascc.map;

import com.example.testmascc.dto.CartDto;
import com.example.testmascc.entity.CartDb;
import com.example.testmascc.entity.CartItemDb;
import org.springframework.stereotype.Component;

@Component

public class CartMapper implements Mapper<CartDb, CartDto> {
    @Override
    public CartDto map(CartDb object) {
        return new CartDto(
                object.getId(),
                object.getItems().stream()
                        .map(CartItemDb::getId)
                        .toList(),
                object.getOwner().getId()
        );
    }
}
