package com.example.testmascc.map;

import com.example.testmascc.dto.ProductDto;
import com.example.testmascc.entity.ProductDb;
import org.springframework.stereotype.Component;

@Component

public class ProductMapper implements Mapper<ProductDb, ProductDto> {
    @Override
    public ProductDto map(ProductDb object) {
        return new ProductDto(
                object.getId(),
                object.getProductName(),
                object.getProductDescription(),
                object.getPrice(),
                object.getCreatedAt()
        );
    }
}
