package com.example.testmascc.map;

import com.example.testmascc.dto.UserDto;
import com.example.testmascc.entity.UserDb;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserMapper implements Mapper<UserDb, UserDto> {
    @Override
    public UserDto map(UserDb object) {
        return new UserDto(
                object.getId(),
                object.getEmail(),
                object.getFirstName(),
                object.getLastName(),
                object.getAddress(),
                object.getPassword(),
                object.getCreatedAt(),
                object.getRole(),
                object.getCart().getId()
        );
    }
}
