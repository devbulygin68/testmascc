package com.example.testmascc.service;

import com.example.testmascc.dto.UserDto;
import com.example.testmascc.entity.CartDb;
import com.example.testmascc.entity.Role;
import com.example.testmascc.entity.UserDb;
import com.example.testmascc.map.UserMapper;
import com.example.testmascc.repository.UserRepository;
import com.example.testmascc.security.CurrentUserProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {
    private final PasswordEncoder passwordEncoder;
    private final CurrentUserProvider currentUserProvider;
    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public Optional<UserDto> getUserById(Long id) {
        return userRepository.findById(id)
                .map(userMapper::map);
    }
    @Transactional
    public UserDto createUser(UserDto userDto) {
        UserDb user = new UserDb();
        user.setEmail(userDto.getEmail());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setAddress(userDto.getAddress());
        user.setRole(Role.USER);

        Optional.ofNullable(userDto.getPassword())
                .filter(StringUtils::hasText)
                .map(passwordEncoder::encode)
                .ifPresent(user::setPassword);
        user.setCart(new CartDb(user));
        userRepository.save(user);
        return userMapper.map(user);
    }


    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserDb user = userRepository.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("Failed to retrieve user with email: " + email));

        String userRole = String.valueOf(user.getRole());

        List<SimpleGrantedAuthority> authorities = List.of(
                new SimpleGrantedAuthority(userRole)
        );

        return new org.springframework.security.core.userdetails.User(
                user.getEmail(), user.getPassword(), authorities
        );
    }

    public UserDb findUserByEmail(String email) {
        return userRepository.findByEmail(email).orElseThrow(
                () -> new UsernameNotFoundException("Failed to retrieve user with email: " + email)
        );
    }

    public UserDb getCurrentUserDb() {
        String currentUserEmail = currentUserProvider.getCurrentUserEmail();
        return userRepository.findByEmail(currentUserEmail).orElseThrow(
                () -> new UsernameNotFoundException(currentUserEmail));
    }

    public UserDto getCurrentUserDto() {
        String currentUserEmail = currentUserProvider.getCurrentUserEmail();
        return userMapper.map(userRepository.findByEmail(currentUserEmail)
                .orElseThrow(
                        () -> new UsernameNotFoundException(currentUserEmail)));
    }

}
