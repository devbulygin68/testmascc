package com.example.testmascc.service;

import com.example.testmascc.dto.CartDto;
import com.example.testmascc.dto.CartItemDto;
import com.example.testmascc.entity.CartDb;
import com.example.testmascc.entity.CartItemDb;
import com.example.testmascc.exception.CartItemNotFoundException;
import com.example.testmascc.map.CartItemMapper;
import com.example.testmascc.repository.CartItemRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class CartItemService {

    private final CartItemRepository cartItemRepository;
    private final ProductService productService;
    private final CartItemMapper cartItemMapper;

    @Transactional
    public CartItemDb addProductToItem(Long productId, Integer quantity, CartDb cart) {
        CartItemDb cartItem = new CartItemDb();
        Optional<CartItemDb> currentItem = cart.getItems().stream()
                .filter(item -> item.getProduct().getId().equals(productId))
                .findFirst();

        if (currentItem.isEmpty()) {
            cartItem.setProduct(productService.getProductDbById(productId));
            cartItem.setQuantity(quantity);
            cartItem.setCart(cart);
        } else {
            cartItem = currentItem.get();
            int quantityItem = cartItem.getQuantity();
            cartItem.setQuantity(quantityItem + 1);
        }

        return cartItemRepository.save(cartItem);
    }

    public CartItemDb getCartItemById(Long cartItemId) {
        return cartItemRepository.findById(cartItemId).orElseThrow(() ->
                new CartItemNotFoundException(cartItemId));
    }

    public List<CartItemDto> getCartItems(CartDto cartDto) {
        return cartDto.getItems().stream()
                .map(this::getCartItemById)
                .map(cartItemMapper::map)
                .toList();
    }

    @Transactional
    public void removeItem(Long cartItemId) {
        cartItemRepository.deleteById(cartItemId);
    }
}
