package com.example.testmascc.service;

import com.example.testmascc.dto.ProductDto;
import com.example.testmascc.entity.ProductDb;
import com.example.testmascc.exception.ProductNotFoundException;
import com.example.testmascc.map.ProductMapper;
import com.example.testmascc.repository.ProductRepository;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;
    private final ProductMapper productMapper;


    public ProductDto getProductDtoById(Long id) {
        return productRepository.findById(id)
                .map(productMapper::map)
                .orElseThrow(() -> new ProductNotFoundException(id));
    }

    public ProductDb getProductDbById(Long id) {
        return productRepository.findById(id)
                .orElseThrow(() -> new ProductNotFoundException(id));
    }


    public List<ProductDto> getProductsByKeyword(String keyword, PageRequest pageRequest) {
        return productRepository.findByKeyword(keyword, pageRequest).stream()
                .map(productMapper::map)
                .toList();
    }

    public Page<ProductDto> getPageProductReadDtoByKeyword(String keyword, Integer page, Integer size) {
        return productRepository.findByKeyword(keyword, PageRequest.of(page, size))
                .map(productMapper::map);
    }

    public Page<ProductDto> getPageAllProductReadDto(Integer page, Integer size) {
        return productRepository.findAll(PageRequest.of(page, size))
                .map(productMapper::map);
    }

    @Transactional
    public ProductDto createProduct(ProductDto productDto) {
        ProductDb product = new ProductDb();
        product.setProductName(productDto.getProductName());
        product.setProductDescription(productDto.getProductDescription());
        product.setPrice(productDto.getPrice());
        productRepository.save(product);
        return productMapper.map(product);

    }

    @Transactional
    public ProductDto updateProductById(
            Long id,
            ProductDto productDto)
            throws ProductNotFoundException {
        ProductDb product = productRepository.findById(id).orElseThrow(
                () -> new ProductNotFoundException(id)
        );
        product.setPrice(productDto.getPrice());
        product.setProductName(productDto.getProductDescription());
        product.setProductDescription(productDto.getProductDescription());
        productRepository.save(product);
        return productMapper.map(product);

    }

    @Transactional
    public void deleteProductById(Long id) {

        productRepository.deleteById(id);
    }

    public Page<ProductDto> getProductPage(HttpServletRequest request) {
        int page = 0;
        int size = 5;
        String keyword;

        if (request.getParameter("page") != null && !request.getParameter("page").isEmpty()) {
            page = Integer.parseInt(request.getParameter("page")) - 1;
        }

        if (request.getParameter("size") != null && !request.getParameter("size").isEmpty()) {
            size = Integer.parseInt(request.getParameter("size"));
        }

        if (request.getParameter("keyword") != null && !request.getParameter("keyword").isEmpty()) {
            keyword = request.getParameter("keyword");

            return getPageProductReadDtoByKeyword(keyword, page, size);
        }

        return getPageAllProductReadDto(page, size);
    }
}



