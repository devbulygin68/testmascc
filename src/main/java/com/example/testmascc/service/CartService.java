package com.example.testmascc.service;

import com.example.testmascc.dto.CartDto;
import com.example.testmascc.dto.CartItemDto;
import com.example.testmascc.entity.CartDb;
import com.example.testmascc.entity.CartItemDb;
import com.example.testmascc.entity.UserDb;
import com.example.testmascc.map.CartMapper;
import com.example.testmascc.repository.CartRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CartService {
    private final CartRepository cartRepository;
    private final UserService userService;
    private final CartMapper cartMapper;
    private final CartItemService cartItemService;

    @Transactional
    public CartDb addCartItemToCart(CartDb cart, CartItemDb cartItem) {
        List<CartItemDb> cartItems = cart.getItems();
        cartItems.add(cartItem);
        return cartRepository.save(cart);
    }

    @Transactional
    public void addProductToCart(Long productId, Integer quantity) {

        CartDb currentUserCart = getCurrentUserCart();
        CartItemDb cartItem = cartItemService.addProductToItem(productId, quantity, currentUserCart);
        cartMapper.map(addCartItemToCart(currentUserCart, cartItem));

    }

    public CartDto getCartDtoByUser(UserDb user) {
        CartDb cart = cartRepository.findById(user.getCart().getId()).orElseThrow(
                () -> new RuntimeException("User's cart " + user.getId() + " not found")
        );
        return cartMapper.map(cart);
    }

    @Transactional
    public void removeItem(Long cartItemId) {
        CartDb cartDb = getCurrentUserCart();
        List<CartItemDb> items = cartDb.getItems();
        CartItemDb item = cartItemService.getCartItemById(cartItemId);
        items.remove(item);
        cartDb.setItems(items);
        cartRepository.save(cartDb);
        cartItemService.removeItem(cartItemId);
    }

    public CartDb getCurrentUserCart() {
        UserDb currentUser = userService.getCurrentUserDb();
        return currentUser.getCart();
    }

    public CartDto getCurrentUserCartDto() {
        UserDb currentUser = userService.getCurrentUserDb();
        return cartMapper.map(currentUser.getCart());
    }

    public List<CartItemDto> getCartProductList() {
        CartDto cartDTO = getCurrentUserCartDto();
        return cartItemService.getCartItems(cartDTO);
    }

}
