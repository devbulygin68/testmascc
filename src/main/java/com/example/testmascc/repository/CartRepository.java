package com.example.testmascc.repository;

import com.example.testmascc.entity.CartDb;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartRepository extends JpaRepository<CartDb, Long> {

}
