package com.example.testmascc.repository;

import com.example.testmascc.entity.CartItemDb;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartItemRepository extends JpaRepository<CartItemDb, Long> {
}
