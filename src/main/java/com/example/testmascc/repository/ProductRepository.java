package com.example.testmascc.repository;


import com.example.testmascc.entity.ProductDb;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface ProductRepository extends JpaRepository<ProductDb, Long> {
    @Query(value = "SELECT * FROM products p WHERE p.product_name LIKE %:keyword% "
            + "OR p.product_description LIKE %:keyword%", nativeQuery = true)
    Page<ProductDb> findByKeyword(@Param("keyword") String keyword, Pageable pageable);
}

