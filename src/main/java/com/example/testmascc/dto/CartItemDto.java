package com.example.testmascc.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CartItemDto {
    private CartDto cartId;
    private ProductDto product;
    private Integer quantity;


}
