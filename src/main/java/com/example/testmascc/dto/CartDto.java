package com.example.testmascc.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class CartDto {

    private Long id;

    private List<Long> items;

    private Long owner;

}
