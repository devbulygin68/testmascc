package com.example.testmascc.dto;


import com.example.testmascc.entity.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    private Long id;
    private String email;
    private String firstName;
    private String lastName;
    private String address;
    private String password;
    private Date createdAt;
    private Role role;
    private Long cartId;
}
