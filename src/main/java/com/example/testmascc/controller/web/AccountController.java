package com.example.testmascc.controller.web;

import com.example.testmascc.dto.UserDto;
import com.example.testmascc.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.security.Principal;

@Controller
@RequiredArgsConstructor
public class AccountController {
    private final UserService userService;
    @GetMapping("/account")

    public String currentUserName(Principal principal, Model model) {
        model.addAttribute("user", userService.findUserByEmail(principal.getName()));
        return "account";
    }
    @GetMapping("/login")
    public String login() {
        return "login";
    }
    @GetMapping("/register")
    public String reg() {
        return "register";
    }
    @PostMapping("/register/save")
    public String registration(@Valid @ModelAttribute("user") UserDto userDto
    ) {
        userService.createUser(userDto);
        return "redirect:/register?success";
    }
}
