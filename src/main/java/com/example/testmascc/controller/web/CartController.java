package com.example.testmascc.controller.web;

import com.example.testmascc.service.CartItemService;
import com.example.testmascc.service.CartService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequiredArgsConstructor
@RequestMapping("/cart")
public class CartController {
    private final CartService cartService;
    private final CartItemService cartItemService;

    @PostMapping("/item")
    public String addProductToCart(@RequestParam("productId") Long productId, @RequestParam("quantity") int quantity) {
        cartService.addProductToCart(productId, quantity);
        return "redirect:/products?success_add";
    }

    @DeleteMapping("item/{id}")
    public String removeCartItem(@PathVariable Long cartItemId) {
        cartService.removeItem(cartItemId);
        return "cart";
    }

    @GetMapping()
    public String getCartProductList(Model model) {

        model.addAttribute("items", cartService.getCartProductList());
        return "cart";
    }
}
