package com.example.testmascc.controller.web;

import com.example.testmascc.dto.ProductDto;
import com.example.testmascc.entity.ProductDb;
import com.example.testmascc.service.ProductService;
import com.example.testmascc.service.UserService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;


@Controller
@RequiredArgsConstructor
@RequestMapping("/products")
public class ProductViewController {
    private final ProductService productService;
    private final UserService userService;


    @PostMapping("/add")
    @PreAuthorize("hasAuthority('ADMIN')")
    public String addProduct(ProductDto product) {
        productService.createProduct(product);
        return "redirect:/products";
    }

    @GetMapping("/add")
    @PreAuthorize("hasAuthority('ADMIN')")
    public String addProductForm(Model model) {
        model.addAttribute("product", new ProductDb());
        return "add-product";
    }


    @GetMapping("/{id}")
    public String getProductById(@PathVariable long id) {
        List<ProductDto> productList = List.of(productService.getProductDtoById(id));
        return "products";
    }

    @GetMapping()
    public String productPage(HttpServletRequest request, Model model) {

        model.addAttribute("currentuser", userService.getCurrentUserDto());

        model.addAttribute("products", productService.getProductPage(request));
        return "products";
    }
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteProduct(@PathVariable long id) {
        productService.deleteProductById(id);
    }
}
