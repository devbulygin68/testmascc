package com.example.testmascc.controller.api;


import com.example.testmascc.dto.ProductDto;
import com.example.testmascc.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import static com.example.testmascc.controller.api.ProductController.PRODUCTS_CONTROLLER_PATH;

@RestController
@RequiredArgsConstructor
@RequestMapping("${base-url}" + PRODUCTS_CONTROLLER_PATH)
public class ProductController {
    public static final String PRODUCTS_CONTROLLER_PATH = "/products";
    public static final String ID = "/{id}";
    private final ProductService productService;
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ProductDto addNewProduct(@RequestBody ProductDto productDto) {
        return productService.createProduct((productDto));
    }

    @GetMapping(ID)
    public ProductDto getProduct(@PathVariable long id) {
        return productService.getProductDtoById(id);
    }
    @PutMapping(ID)
    @ResponseStatus(HttpStatus.OK)
    public ProductDto updateProduct(
            @PathVariable long id,
            @RequestBody ProductDto productDto) {
        return productService.updateProductById(id, productDto);
    }
    @DeleteMapping(ID)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteProduct(@PathVariable long id) {
        productService.deleteProductById(id);
    }
}
