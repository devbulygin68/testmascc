package com.example.testmascc.controller.api;


import com.example.testmascc.dto.UserDto;
import com.example.testmascc.repository.UserRepository;
import com.example.testmascc.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("${base-url}" + UserController.USER_CONTROLLER_PATH)
public class UserController {
    public static final String USER_CONTROLLER_PATH = "/users";
    public static final String ID = "/{id}";
    private UserRepository userRepository;

    private UserService userService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public UserDto regNewUser(@RequestBody UserDto userDto) {
        return userService.createUser(userDto);
    }


    @GetMapping(ID)
    public Optional<UserDto> getUser(@PathVariable long id) {
        return userService.getUserById(id);
    }

}
