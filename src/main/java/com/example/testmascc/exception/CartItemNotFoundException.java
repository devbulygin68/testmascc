package com.example.testmascc.exception;

public class CartItemNotFoundException extends RuntimeException {
    public CartItemNotFoundException(Long id) {
        super("Could not find item with id " + id);
    }
}
