CREATE TABLE IF NOT EXISTS product
(
    id                 SERIAL PRIMARY KEY,
    product_name        VARCHAR(255) NOT NULL,
    product_description TEXT,
    price              NUMERIC(10, 2) NOT NULL,
    quantity           INT            NOT NULL,
    created_at          TIMESTAMP
);
