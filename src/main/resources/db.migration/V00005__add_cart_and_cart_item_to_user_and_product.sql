ALTER TABLE users
    ADD COLUMN cart_id BIGINT;

ALTER TABLE users
    ADD CONSTRAINT fk_cart
        FOREIGN KEY (cart_id)
            REFERENCES сart (id);

ALTER TABLE products
    ADD COLUMN product_id BIGINT;

ALTER TABLE products
    ADD CONSTRAINT fk_cart_item
        FOREIGN KEY (product_id)
            REFERENCES сart_item (id);