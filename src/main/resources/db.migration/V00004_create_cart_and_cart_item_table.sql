CREATE TABLE сart
(
    id       SERIAL PRIMARY KEY,
    owner_id BIGINT,
    FOREIGN KEY (owner_id) REFERENCES users (id)
);

CREATE TABLE сart_item
(
    id         SERIAL PRIMARY KEY,
    cart_id    BIGINT NOT NULL,
    product_id BIGINT,
    quantity   INTEGER,
    FOREIGN KEY (cart_id) REFERENCES сart (id),
    FOREIGN KEY (product_id) REFERENCES products (id)
);
