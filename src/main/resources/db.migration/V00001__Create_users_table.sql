CREATE TABLE IF NOT EXISTS users
(
    id
    SERIAL
    PRIMARY
    KEY,
    email
    VARCHAR
(
    255
) NOT NULL UNIQUE,
    first_name VARCHAR
(
    255
),
    last_name VARCHAR
(
    255
),
    address VARCHAR
(
    255
),
    password VARCHAR
(
    255
) NOT NULL,
    created_at TIMESTAMP
    );
